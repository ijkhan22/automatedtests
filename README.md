# Automated Tests



## Getting started

This project provides automated testing for a website using Selenium and displays the results using Allure.

## Features
This testing suite includes the following tests:
1. Check if buttons function correctly on the home page.
2. Test the compatibility of the home page across different devices.
3. Check the page load time for different links within the website.
4. Validate the navigation links in the website's navigation bar.
5. Check the loading time and visibility of images on the website.

Things not tested
1. Session checks (as there is no login functionality).
2. Form submissions (to prevent spamming the email list).

## Demo
Watch this video demonstration on how to run the script and see a sample output:
[profilance.mp4](sampleresults%2Fprofilance.mp4)

Here are some sample results showing the test results and trends:

![2023-07-25_22-14-44.png](sampleresults%2F2023-07-25_22-14-44.png)
![2023-07-25_22-28-49.png](sampleresults%2F2023-07-25_22-28-49.png)
![2023-07-25_22-29-49.png](sampleresults%2F2023-07-25_22-29-49.png)

## Installation

Follow these steps to set up and run the testing suite:

1. **Setup Python Environment**
   - Download and install Python from the [official website](https://www.python.org/downloads/).

2. **Install Required Packages**
   - Use pip to install necessary packages with the provided requirements file. Run the following command in your terminal:
     ```
     pip install -r requirements.txt
     ```

3. **Setup Allure**
   - Download and install Allure from the [official website](https://docs.qameta.io/allure/).

4. **Run the Script**
   - Run the following commands in your terminal to start the testing suite:
     ```
     pytest --alluredir=/tmp/my_allure_results
     allure serve /tmp/my_allure_results
     ```