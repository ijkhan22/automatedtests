"""
this python scripts implements five automated tests for a website
"""

import allure
import pytest
from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.common.action_chains import ActionChains

def wait_for_clickable_element(driver, locator):
    """
    Wait for an element to be clickable.
    :return: The WebElement
    """
    return WebDriverWait(driver, 10).until(EC.element_to_be_clickable(locator))


def wait_for_element_present(driver, locator):
    """
    Wait for an element to be present on the web page.
    :return:The WebElement
    """
    return WebDriverWait(driver, 10).until(EC.presence_of_element_located(locator))


# Test 1

url_data = [
    {"button": "Learn more", "expected_url": "https://profilence.com/solutions/solutions-overview/"}
    , {"button": "Demo request", "expected_url": "https://profilence.com/demo-request/"}
]


@allure.feature('Website Navigation')
@allure.story('Check Button URLs')
@pytest.mark.parametrize('item', url_data)
def test_check_button_urls(item):
    """
     Test case to check the URLs of buttons on the website.
    :param item: test data, button name and expected url
    Assertions:
        - The test will fail if the current URL after clicking the button does not match the expected URL.
    Allure Report:
        - The test result, along with the current URL, will be recorded in the Allure report.
    """
    driver = webdriver.Chrome()
    driver.get("https://profilence.com")

    with allure.step(f"Click on '{item['button']}' button"):
        demo_request_button_locator = (By.LINK_TEXT, item['button'])
        demo_request_button = wait_for_clickable_element(driver, demo_request_button_locator)
        demo_request_button.click()

        # Wait for the new page to fully load
        WebDriverWait(driver, 10).until(EC.url_to_be(item["expected_url"]))

        current_url = driver.current_url
        print("Current URL:", current_url)
        assert current_url == item["expected_url"]
        allure.attach(driver.current_url, name="Current URL", attachment_type=allure.attachment_type.TEXT)

    driver.quit()


# Test 2


device_data = [
    {"device": "iPhone X", "width": 375, "height": 812}
    , {"device": "iPad Pro", "width": 1024, "height": 1366},
    {"device": "Laptop - 1366x768", "width": 1366, "height": 768},
]

expected_elements = [
    (By.LINK_TEXT, "Demo request")
    , (By.LINK_TEXT, "Learn more")
]


@allure.feature('Home Page Compatibility')
@allure.story('Check Home Page Compatibility for Different Devices')
def test_home_page_compatibility():
    """
    Test the compatibility of the home page on different devices.
    the test will iterate through list of devices and open the website,
    and will check if the expected element is present and clickable
        Assertions:
        - The test will fail if the element is not found or is not clickable.
    Allure Report:
        - The test result, along with the device name, will be recorded in the Allure report.
    """
    for device in device_data:
        driver = webdriver.Chrome()
        driver.set_window_size(device['width'], device['height'])
        driver.get("https://profilence.com")

        with allure.step(f"Check home page compatibility for {device['device']}"):
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.TAG_NAME, "body")))
            for element in expected_elements:
                wait_for_clickable_element(driver, element)

        driver.quit()


# Test 3

link_data = [
    {"link": "https://profilence.com/"},
    {"link": "https://profilence.com/industries/automotive/"}
]


@allure.feature('Website Performance')
@allure.story('Check Page Load Times')
@pytest.mark.parametrize('item', link_data)
def test_page_load_time(item):
    """
    check the page load times of given links.
    parse link_data list and open all the links, check page load time
    Assertions:
        - The test will fail if the page load time is greater than 3 seconds
    Allure Report:
        - The test result, along with the page link, will be recorded in the Allure report.
    """
    driver = webdriver.Chrome()

    start_time = time.time()
    driver.get(item['link'])
    end_time = time.time()

    load_time = end_time - start_time

    with allure.step(f"Check load time for {item['link']}"):
        print(f"Page {item['link']} took {load_time} seconds to load.")
        if load_time > 3:
            print(f"WARNING: Page {item['link']} took longer than 3 seconds to load.")

    driver.quit()


# Test 4

navigation_link = "//span[normalize-space()='{name}']"
nav_items = [
    ("Home", "//span[normalize-space()='Home']"),
    ("Industries", "//button[normalize-space()='Industries']"),
    ("Automotive", "(//span[contains(@class,'mr-10 md:mr-15 lg:mr-25')][normalize-space()='Automotive'])[2]")
]


@allure.feature('Website Navigation')
@allure.story('Check Navigation Bar Links')
def test_navigation_bar_links():
    """
    Test the navigation links in the website's navigation bar.
    The test will iterate through each navigation bar item and will click it to see its working
    Assertions:
        - The test will fail if the element is not found or is not clickable.
    Allure Report:
        - The test result, along with the device name, will be recorded in the Allure report.
    """
    driver = webdriver.Chrome()
    base_url = "https://profilence.com"
    driver.get(base_url)

    for nav_item, xpath in nav_items:
        with allure.step(f"Check '{nav_item}' link"):
            link_click = False

            try:
                # Wait for the element to be clickable before clicking it
                link = wait_for_clickable_element(driver, (By.XPATH, xpath))
                link.click()
                link_click = True
            except Exception as e:
                pass

            assert link_click is True

    driver.quit()


# Test 5
images = [
    ("DemoImage", '/html/body/div/div[1]/main/div/section[1]/div[2]/div/picture/img'),
    ("WhyUsImage", "/html/body/div[2]/div[1]/main/div/section[2]/div/div/div[1]/div/picture/img")
]


@allure.feature('Image Loading')
@allure.story('Check Image Loading on the Website')
def test_image_loading():
    """
    check the loading and visibility of images on a website.
    the method will open images on homepage and will see if they are being displayed
    Assertions:
        - The test will fail if image is not found or is not being displayed
    Allure Report:
        - The test result, along with the image screenshot, will be recorded in the Allure report.
    """
    driver = webdriver.Chrome()
    driver.get("https://profilence.com")

    for image_name, image_xpath in images:
        with allure.step(f"Check {image_name} image"):
            try:
                image_element = driver.find_element(By.XPATH, image_xpath)

                assert image_element.is_displayed(), f"{image_name} image is not displayed."

                # Take a screenshot of the image and attach it to the Allure report
                screenshot = image_element.screenshot_as_png
                allure.attach(screenshot, name=f"{image_name} Image", attachment_type=allure.attachment_type.PNG)
            except NoSuchElementException:
                allure.attach("", name=f"{image_name} Image", attachment_type=allure.attachment_type.TEXT)
                assert False, f"{image_name} image not found."

    driver.quit()

# # """
# pytest --alluredir=/tmp/my_allure_results
# allure serve /tmp/my_allure_results
# # """
